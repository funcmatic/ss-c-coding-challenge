## Coding Challange Test Task
performed by Anton Rybak

### GNode
run:  
`./mvnw spring-boot:run -f "./gnode/"`


### Word Count
run using LICENSE.txt:  
`./mvnw spring-boot:run -f "./wordcount/" -Dspring-boot.run.arguments="./LICENSE.txt"`
