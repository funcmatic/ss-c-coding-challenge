package ssc.java.challenge;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@SpringBootApplication
public class WordCountSpringApp implements CommandLineRunner {

    private static final String MESSAGE_FILEPATH_ERROR = "Please provide a valid path to text file.";
    private static final String MESSAGE_PROCESSING_FILE = "\nProcessing your file:\n";
    private static final String MESSAGE_OUTPUT_SEPARATOR = "\t";

    private WordCountApp app;

    public WordCountSpringApp(WordCountApp wordCountApp) {
        this.app = wordCountApp;
    }

    public static void main(String[] args) {
        SpringApplication.run(WordCountSpringApp.class, args);
	}

    public void run(String... args) {
        try {
            //argument checks
            if (args.length == 0) {
                throw new IllegalArgumentException(MESSAGE_FILEPATH_ERROR);
            }
            //filepath checks
            Path filepath = Paths.get(args[0]).normalize().toAbsolutePath();
            if (Files.notExists(filepath) || !Files.isRegularFile(filepath)) {
                throw new IllegalArgumentException(MESSAGE_FILEPATH_ERROR);
            }
            //process file
            System.out.println(MESSAGE_PROCESSING_FILE + filepath);
            app.countWords(Files.readAllLines(filepath))
                    .forEach((key, val) -> System.out.println(val + MESSAGE_OUTPUT_SEPARATOR + key));

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
