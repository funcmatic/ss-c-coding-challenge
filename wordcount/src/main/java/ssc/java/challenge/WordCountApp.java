package ssc.java.challenge;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class WordCountApp {

    private static final String REGEX_NOT_A_WORD = "[^\\w+]";

    /**
     * Counts words in specified list of String objects.
     *
     * @param lines to process
     * @return map where keys is words and values is their count
     * @throws NullPointerException when any element of list or list itself is null.
     */
    public Map<String, Long> countWords(List<String> lines) {
        return lines.stream()
                .flatMap(line -> Arrays.stream(line.toLowerCase().split(REGEX_NOT_A_WORD)))
                .filter(word -> !word.isEmpty())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }
}
