package ssc.java.challenge;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;


public class WordCountAppTest {

    WordCountApp wordCountApp = new WordCountApp();

    @Test
    public void whenEmptyListProvidedThenMapSizeIs0() {
        //given
        List<String> lines = Collections.emptyList();
        //when
        Map<String, Long> actual = wordCountApp.countWords(lines);
        //then
        assertEquals(0, actual.size());
    }

    @Test
    public void whenOneWordProvidedThenMapSizeIs1() {
        //given
        List<String> lines = Collections.singletonList("word");
        //when
        Map<String, Long> actual = wordCountApp.countWords(lines);
        //then
        assertEquals(1, actual.size());
    }

    @Test
    public void whenTwoWordsProvidedThenMapSizeIs2() {
        //given
        List<String> lines = Collections.singletonList("Some word");
        //when
        Map<String, Long> actual = wordCountApp.countWords(lines);
        //then
        assertEquals(2, actual.size());
    }

    @Test
    public void whenWordsProvidedInLowerAndUpperCasesThenTheyCountAsOneWordInLowerCase() {
        //given
        List<String> lines = Collections.singletonList("word WORD WoRd WorD");
        //when
        Map<String, Long> actual = wordCountApp.countWords(lines);
        //then
        assertEquals(4, actual.get("word").longValue());
    }

    @Test
    public void whenWordsProvidedInSeparateLinesThenTheyCount() {
        //given
        List<String> lines = Arrays.asList("word WORD", "WoRd", " WorD");
        //when
        Map<String, Long> actual = wordCountApp.countWords(lines);
        //then
        assertEquals(4, actual.get("word").longValue());
    }

    @Test
    public void whenDifferentWordsProvidedThenAllRecognizedProperly() {
        //given
        List<String> lines = Arrays.asList("Some word", "Some other words", "some woRD");
        //when
        Map<String, Long> actual = wordCountApp.countWords(lines);
        //then
        assertEquals(3L, actual.get("some").longValue());
        assertEquals(2L, actual.get("word").longValue());
        assertEquals(1L, actual.get("words").longValue());
    }

    @Test
    public void whenWordsSeparatedByNonSpaceCharacterThenAllWordsRecognizedProperly() {
        //given
        List<String> lines = Arrays.asList("word,word:", ";word)%$#%word");
        //when
        Map<String, Long> actual = wordCountApp.countWords(lines);
        //then
        assertEquals(4L, actual.get("word").longValue());
    }

    @Test(expected = NullPointerException.class)
    public void whenNullProvidedNullPointerExceptionThrown() {
        //when
        Map<String, Long> actual = wordCountApp.countWords(null);
    }

    @Test(expected = NullPointerException.class)
    public void whenListWithNullElementProvidedNullPointerExceptionThrown() {
        //given
        List<String> stringList = Arrays.asList("some string", null);
        //when
        Map<String, Long> actual = wordCountApp.countWords(stringList);
    }
}