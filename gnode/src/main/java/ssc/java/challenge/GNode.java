package ssc.java.challenge;

public interface GNode {

    String getName();

    GNode[] getChildren();
}
