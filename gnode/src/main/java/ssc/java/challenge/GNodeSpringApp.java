package ssc.java.challenge;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.StringJoiner;

@SpringBootApplication
public class GNodeSpringApp implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(GNodeSpringApp.class, args);
    }

    @Override
    public void run(String... args) {
        //test task sample data
        GNodeDemoFake I = new GNodeDemoFake("I");
        GNodeDemoFake H = new GNodeDemoFake("H");
        GNodeDemoFake G = new GNodeDemoFake("G");
        GNodeDemoFake F = new GNodeDemoFake("F");
        GNodeDemoFake E = new GNodeDemoFake("E");
        GNodeDemoFake D = new GNodeDemoFake("D");
        GNodeDemoFake C = new GNodeDemoFake("C", G, H, I);
        GNodeDemoFake B = new GNodeDemoFake("B", E, F);
        GNodeDemoFake A = new GNodeDemoFake("A", B, C, D);

        //walkGraph demo
        StringJoiner stringJoiner = new StringJoiner(", ", "\nwalkGraph():\n", "\n");
        GNodeUtils.walkGraph(A).forEach(o -> stringJoiner.add(o.toString()));
        System.out.println(stringJoiner.toString());

        //paths demo
        System.out.println("paths():\n" + GNodeUtils.paths(A).toString());
    }
}
