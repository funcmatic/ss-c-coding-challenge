package ssc.java.challenge;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class GNodeUtils {
    /**
     * Collects all nodes of specified Graph root
     * using Breath-First Traversal.
     *
     * @param node root node
     * @return list of nodes
     */
    public static List<GNode> walkGraph(GNode node) {
        ArrayList<GNode> result = new ArrayList<>();
        LinkedList<GNode> queue = new LinkedList<>();
        queue.add(node);
        while (!queue.isEmpty()) {
            node = queue.removeFirst();
            result.add(node);
            for (GNode child : node.getChildren()) {
                if (!result.contains(child)) {
                    queue.addLast(child);
                }
            }
        }
        return result;
    }

    /**
     * Collects all paths from specified Graph root
     * using Depth-First Traversal. (recursive)
     *
     * @param node root node
     * @return list of possible paths
     */
    public static List paths(GNode node) {
        ArrayList<List<GNode>> paths = new ArrayList<>();
        LinkedList<GNode> stack = new LinkedList<>();
        addPath(node, stack, paths);
        return paths;
    }

    private static void addPath(GNode node, Deque<GNode> stack, List<List<GNode>> paths) {
        stack.add(node);
        GNode[] children = node.getChildren();
        if (children.length != 0) {
            for (GNode childNode : children) {
                addPath(childNode, stack, paths);
            }
        } else {
            paths.add(new ArrayList<>(stack));
        }
        stack.removeLast();
    }
}
