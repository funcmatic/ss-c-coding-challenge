package ssc.java.challenge;

/**
 * Fake implementation for the demo app.
 */
public class GNodeDemoFake implements GNode {

    private String name;
    private GNode[] children;

    public GNodeDemoFake(String name) {
        this.name = name;
        this.children = new GNode[0];
    }

    public GNodeDemoFake(String name, GNode... children) {
        this.name = name;
        this.children = children;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public GNode[] getChildren() {
        return children;
    }

    @Override
    public String toString() {
        return name;
    }
}
