package ssc.java.challenge;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * I'm using GNodeDemoFake for demo app as well as for tests.
 */
public class GNodeUtilsTest {

    // ----------- walkGraph() tests -----------
    @Test
    public void whenGraphIsSingleNodeThenArraySizeIsOne() {
        //given
        GNode root = new GNodeDemoFake("root");
        //when
        int actual = GNodeUtils.walkGraph(root).size();
        //then
        assertEquals(1, actual);
    }

    @Test
    public void whenRootHasOneChildThenArraySizeIsTwo() {
        //given
        GNode child = new GNodeDemoFake("child");
        GNode root = new GNodeDemoFake("root", child);
        //when
        int actual = GNodeUtils.walkGraph(root).size();
        //then
        assertEquals(2, actual);
    }

    @Test
    public void whenRootHasTwoChildrenThenArraySizeIsThree() {
        //given
        GNode firstChild = new GNodeDemoFake("firstChild");
        GNode secondChild = new GNodeDemoFake("secondChild");
        GNode root = new GNodeDemoFake("root", firstChild, secondChild);
        //when
        int actual = GNodeUtils.walkGraph(root).size();
        //then
        assertEquals(3, actual);
    }

    @Test
    public void whenNodeHasChildAndGrandChildThenArraySizeIsThree() {
        //given
        GNode grandChild = new GNodeDemoFake("grandChild");
        GNode firstChild = new GNodeDemoFake("firstChild", grandChild);
        GNode root = new GNodeDemoFake("root", firstChild);
        //when
        int actual = GNodeUtils.walkGraph(root).size();
        //then
        assertEquals(3, actual);
    }

    @Test
    public void whenBothChildrenHasOwnChildrenThenArraySizeIsFive() {
        //given
        GNode firstGrandChild = new GNodeDemoFake("firstGrandChild");
        GNode secondGrandChild = new GNodeDemoFake("secondGrandChild");
        GNode firstChild = new GNodeDemoFake("firstChild", firstGrandChild);
        GNode secondChild = new GNodeDemoFake("secondChild", secondGrandChild);
        GNode root = new GNodeDemoFake("root", firstChild, secondChild);
        //when
        int actual = GNodeUtils.walkGraph(root).size();
        //then
        assertEquals(5, actual);
    }

    @Test
    public void whenNodeHasChildAndGrandChildThenRootIsPresentInList() {
        //given
        GNode grandChild = new GNodeDemoFake("grandChild");
        GNode firstChild = new GNodeDemoFake("firstChild", grandChild);
        GNode root = new GNodeDemoFake("root", firstChild);
        //when
        List actual = GNodeUtils.walkGraph(root);
        //then
        assertTrue(actual.contains(root));
    }

    @Test
    public void whenNodeHasChildAndGrandChildThenChildIsPresentInList() {
        //given
        GNode grandChild = new GNodeDemoFake("grandChild");
        GNode firstChild = new GNodeDemoFake("firstChild", grandChild);
        GNode root = new GNodeDemoFake("root", firstChild);
        //when
        List actual = GNodeUtils.walkGraph(root);
        //then
        assertTrue(actual.contains(firstChild));
    }

    @Test
    public void whenNodeHasChildAndGrandChildThenGrandChildIsPresentInList() {
        //given
        GNode grandChild = new GNodeDemoFake("grandChild");
        GNode firstChild = new GNodeDemoFake("firstChild", grandChild);
        GNode root = new GNodeDemoFake("root", firstChild);
        //when
        List actual = GNodeUtils.walkGraph(root);
        //then
        assertTrue(actual.contains(grandChild));
    }

    // ----------- paths() tests -----------
    @Test
    public void whenSingleNodeInGraphThenOnePathsReturned() {
        //given
        GNode root = new GNodeDemoFake("root");
        //when
        int actual = GNodeUtils.paths(root).size();
        //then
        assertEquals(1, actual);
    }

    @Test
    public void whenBothChildrenHasOwnChildrenThenTwoPathsReturned() {
        //given
        GNode firstGrandChild = new GNodeDemoFake("firstGrandChild");
        GNode secondGrandChild = new GNodeDemoFake("secondGrandChild");
        GNode firstChild = new GNodeDemoFake("firstChild", firstGrandChild);
        GNode secondChild = new GNodeDemoFake("secondChild", secondGrandChild);
        GNode root = new GNodeDemoFake("root", firstChild, secondChild);
        //when
        int actual = GNodeUtils.paths(root).size();
        //then
        assertEquals(2, actual);
    }

    @Test
    public void whenBothChildrenHasTwoOwnChildrenThenFourPathsReturned() {
        //given
        GNode firstGrandChild = new GNodeDemoFake("firstGrandChild");
        GNode secondGrandChild = new GNodeDemoFake("secondGrandChild");
        GNode thirdGrandChild = new GNodeDemoFake("firstGrandChild");
        GNode fourthGrandChild = new GNodeDemoFake("secondGrandChild");
        GNode firstChild = new GNodeDemoFake("firstChild", firstGrandChild, secondGrandChild);
        GNode secondChild = new GNodeDemoFake("secondChild", thirdGrandChild, fourthGrandChild);
        GNode root = new GNodeDemoFake("root", firstChild, secondChild);
        //when
        int actual = GNodeUtils.paths(root).size();
        //then
        assertEquals(4, actual);
    }
}